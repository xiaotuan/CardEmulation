/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.qty.cardemulation;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

/**
 * Utility class for persisting account numbers to disk.
 *
 * <p>The default SharedPreferences instance is used as the backing storage. Values are cached
 * in memory for performance.
 *
 * <p>This class is thread-safe.
 */
public class AccountStorage {
    private static final String PREF_ACCOUNT_NUMBER = "account_number";
    private static final String PREF_ACCOUNT_AMOUNT = "account_amount";
    private static final String PREF_ACCOUNT_EXPENSES_RECORD = "expenses_record";
    private static final String DEFAULT_ACCOUNT_NUMBER = "00000000";
    private static final int MAX_RECORD = 5;
    private static final String TAG = "AccountStorage";
    private static String sAccount = null;
    private static final Object sAccountLock = new Object();

    public static void SetAccount(Context c, String s) {
        synchronized(sAccountLock) {
            Log.i(TAG, "Setting account number: " + s);
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
            prefs.edit().putString(PREF_ACCOUNT_NUMBER, s).commit();
            sAccount = s;
        }
    }

    public static String GetAccount(Context c) {
        synchronized (sAccountLock) {
            if (sAccount == null) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(c);
                String account = prefs.getString(PREF_ACCOUNT_NUMBER, DEFAULT_ACCOUNT_NUMBER);
                sAccount = account;
            }
            return sAccount;
        }
    }

    public static float getAmount(Context c) {
        synchronized (sAccountLock) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
            float amount = sp.getFloat(PREF_ACCOUNT_AMOUNT, 100.0f);
            return amount;
        }
    }

    public static void echarge(Context c, double sum) {
        Log.d(TAG, "echarge=>sum: " + sum);
        synchronized (sAccountLock) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
            double amount = sp.getFloat(PREF_ACCOUNT_AMOUNT, 100.0f);
            amount += sum;
            sp.edit().putFloat(PREF_ACCOUNT_AMOUNT, (float)amount).apply();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            String record = sdf.format(calendar.getTime()) + " 充值金额: " + sum;
            String expensesRecord = sp.getString(PREF_ACCOUNT_EXPENSES_RECORD, "");
            Log.d(TAG, "echarge=>expensesRecord: " + expensesRecord);
            if (!TextUtils.isEmpty(expensesRecord)) {
                String[] records = expensesRecord.split(";");
                ArrayList<String> list = new ArrayList<String>();
                list.addAll(Arrays.asList(records));
                Log.d(TAG, "echarge=>size: " + list.size());
                if (list.size() < MAX_RECORD) {
                    list.add(0, record);
                } else {
                    list.add(0, record);
                    list.remove(list.size() - 1);
                }
                String result = "";
                for (int i = 0; i < list.size(); i++) {
                    result += list.get(i);
                    if (i + 1 < list.size()) {
                        result += ";";
                    }
                }
                sp.edit().putString(PREF_ACCOUNT_EXPENSES_RECORD, result).apply();
            } else {
                sp.edit().putString(PREF_ACCOUNT_EXPENSES_RECORD, record).apply();
            }
        }
    }

    public static void consumption(Context c, double sum) {
        synchronized (sAccountLock) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
            double amount = sp.getFloat(PREF_ACCOUNT_AMOUNT, 100.0f);
            amount -= sum;
            sp.edit().putFloat(PREF_ACCOUNT_AMOUNT, (float)amount).apply();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar calendar = Calendar.getInstance();
            String record = sdf.format(calendar.getTime()) + " 消费金额: " + sum;
            String expensesRecord = sp.getString(PREF_ACCOUNT_EXPENSES_RECORD, "");
            Log.d(TAG, "addExpensesRecord=>expensesRecord: " + expensesRecord);
            if (!TextUtils.isEmpty(expensesRecord)) {
                String[] records = expensesRecord.split(";");
                ArrayList<String> list = new ArrayList<String>();
                list.addAll(Arrays.asList(records));
                Log.d(TAG, "addExpensesRecord=>size: " + list.size());
                if (list.size() < MAX_RECORD) {
                    list.add(0, record);
                } else {
                    list.add(0, record);
                    list.remove(list.size() - 1);
                }
                String result = "";
                for (int i = 0; i < list.size(); i++) {
                    result += list.get(i);
                    if (i + 1 < list.size()) {
                        result += ";";
                    }
                }
                sp.edit().putString(PREF_ACCOUNT_EXPENSES_RECORD, result).apply();
            } else {
                sp.edit().putString(PREF_ACCOUNT_EXPENSES_RECORD, record).apply();
            }
        }
    }

    public static String getExpensesRecord(Context c) {
        synchronized (sAccountLock) {
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(c);
            String expensesRecord = sp.getString(PREF_ACCOUNT_EXPENSES_RECORD, "");
            return expensesRecord;
        }
    }
}
